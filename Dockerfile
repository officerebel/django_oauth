# Python version
FROM python:3.7-alpine

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set work directory
WORKDIR /code

# Copy Pipfile
COPY Pipfile /code

# Install dependencies
RUN pip install pipenv
RUN pipenv install --system

# Copy files
COPY . /codeFROM python:3.7-alpine
